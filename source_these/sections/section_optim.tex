\chapter{Problèmes d'optimisation}

\paragraph{Introduction}
Ce chapitre introduit les problèmes d'optimisation. Il se divise en trois parties. La première partie définit formellement les problèmes d'optimisation.  La seconde présente différentes typologies associées aux problèmes d'optimisation en se focalisant sur les problèmes numériques. La dernière partie détaille les méthodes de résolution de problèmes d'optimisation à l'aide des méta heuristiques. Après une courte définition, un panorama d'algorithmes de la littérature pour la résolution de problème d'optimisation numérique boîte noire utilisant une stratégie d'évolution utilisant une stratégie de \emph{restart} est présenté.

\section{Problèmes d'optimisation}
    Résoudre un sodoku, ranger ses affaires dans sa valise, trouver les 
    proportions idéales pour réaliser son pain, définir le chemin  le plus court entre deux villes \dots Voilà bien quelques exemples de problèmes du quotidien dont la résolution est non triviale. Afin de les résoudre, un formalisme possible consiste à reformuler mathématiquement le problème rencontré sous forme d'un modèle. Ainsi l'ensemble des choix possibles (par exemple: l'ensemble des combinaisons de rangement possible des affaires dans le sac, les quantités d'ingrédients de la recette du pain \dots) forment \emph{l'espace de recherche}. Cependant toutes les solutions ne se valent pas forcément, certaines étant plus pertinentes que d'autres en fonction d'un critère d'évaluation. Ce critère défini par la personne voulant résoudre ce problème est appelé \emph{fonction objectif}\footnote{Aussi appelé fonction de coût} et attribue une note pour chaque solution de l'espace de recherche. Cette note est arbitraire et reflète la pertinence d'une solution.
    Lorsque la fonction objective consiste à répondre au(x) critère(s) d'un seul objectif, la solution satisfaisant le mieux l'objectif est alors appelée solution optimale ou \emph{optimum}. Il est possible que les solutions ne puissent pas être comparées, dans ce cas la réponse au problème est un ensemble de solutions. En prenant l'exemple de la recette du pain, on s'aperçoit qu'une solution fait intervenir différents ingrédients tels que l'eau, la farine, le sel, la levure, \dots Ces éléments forment alors la \emph{dimension} du problème. Une solution peut alors être exprimée comme un vecteur où chaque nombre réel correspond à une quantité d'un ingrédient. Cette modélisation a pour but d'opérer une abstraction de la réalité et donc de simplifier un problème parfois complexe à appréhender (réduction du 
    bruit, des défauts, des biais \dots). La conception d'un modèle adéquat repose sur une bonne connaissance experte du domaine considéré ainsi qu'une connaissance des méthodes de résolution. Dès lors, la définition du problème originel en un problème mathématique est faite, il est alors possible de considérer sa résolution comme un problème d'optimisation.
    De manière formelle un problème d'optimisation peut être défini par :
    \begin{itemize}
        \item Un espace de recherche $\chi$ représentant l'ensemble des 
        solutions possibles.
        \item Une fonction objective $f$ étant le critère d'évaluation d'une 
        solution tel que: 
        $ f : \chi \rightarrow \mathbb{R}$
    \end{itemize}
    Ainsi résoudre un problème d'optimisation consiste à trouver la meilleure 
    (en minimisation ou maximisation) solution $x \in \chi$ en fonction de la fonction objective $f$ telle que définie par l'équation~\ref{eq:optim} (ici en minimisation) où $x$ est une solution appartenant à l'espace de recherche $\chi$. 
    \begin{equation}
        x^* = arg min(f)
        \label{eq:optim}
    \end{equation}
    %Avec  $x$, un vecteur composé de $n \geq 1$ paramètres de la solution tel que $x = {x_1, x_2, \dots, x_n}$.

    \section{Typologies d'optimisation}
    Il existe de nombreuses applications utilisant les problèmes d'optimisation. Cependant il est possible de les classer en 2 grandes catégories:
    \begin{itemize}
        \item Mono-objectif. La fonction objective consiste à satisfaire 1 
        critère (ou bien une somme de plusieurs critères). 
        \item Multi objectif. La fonction objective consiste à satisfaire 
        plusieurs critères conjointement.  
    \end{itemize} 
    Chacune de ces catégories possède des typologies (optimisation combinatoire/numérique, boîte noire/ boîte blanche, optimisation dynamique ou non, multimodalité, \dots) qui leur sont propres et influencent le choix des méthodes de résolution. Les travaux de cette thèse ont porté exclusivement sur les problèmes d'optimisation mono-objectif. La suite de la section présente donc les différentes caractéristiques des problèmes d'optimisation continue  multimodale mono-objectif rencontrés au cours de la thèse dans un contexte hors ligne et dynamique.

    \subsection{Classes d'optimisation}
    Les problèmes d'optimisation peuvent être représentés comme une fonction 
    faisant correspondre les solutions d'un espace de recherche avec les 
    résultats de la fonction objectif correspondants. En fonction du type de valeur des variables composant les solutions, le problème peut être classé dans 2 classes d'optimisation. Les travaux de la thèse ainsi que ses contributions n'ont porté que sur des problèmes d'optimisation numérique.

    \subsubsection{Optimisation combinatoire}
    L'espace de recherche est composé de solutions ne contenant que des variables discrètes (appartenant aux entiers). Il est donc potentiellement fini est dénombrable (même si les problèmes combinatoires peuvent être de cardinalité infinie). À ce titre, toutes les solutions pouvant être listées, il suffirait de sélectionner la meilleure pour résoudre le problème. Cette assertion n'est réalisable que si l'espace de recherche est fini et de faible dimension ou bien si l'on dispose d'un budget de temps conséquent~\cite{Francisco} (pour certains problèmes le temps de dénombrement de toutes les solutions peut être plus grand que le nombre d’années depuis le début de l’univers). La plupart des problèmes combinatoires étant NP-difficile~\cite{Papadimitriou1998}, l'utilisation de méthodes de recherche efficaces comme les méthodes exactes ou d'autres méthodes au moins meilleures que l'énumération de toutes les solutions possibles devient nécessaire~\cite{zhao2011feature}. Comme exemples d'applications des problèmes d'optimisation combinatoire, on peut citer :
    \begin{itemize}
        \item La coloration de graphe~\cite{cao2019graph}
        \item Le réglage de feux tricolores pour la mobilité 
        urbaine~\cite{Lepretre2019}
        \item Le pilotage de réacteur nucléaire~\cite{Muniglia2017}
    \end{itemize}
         
    \subsubsection{Optimisation numérique}
    Ce genre d'optimisation est aussi appelée optimisation continue. À l'instar de l'optimisation combinatoire, l'ensemble des solutions ainsi que les valeurs de la fonction de coût appartiennent aux réels. Dans ce cas de figure, le dénombrement des solutions est impossible puisque les variables à optimiser peuvent prendre tout un continuum de valeurs. Comme expliqué dans la sous-section précédente, la contrainte de temps est aussi importante: on souhaite déterminer une solution satisfaisante avec un budget de temps raisonnable. Ainsi les méthodes de résolution d'optimisation continue relèvent toutes de l'analyse des fonctions objectif (lorsque cela est possible) ou bien à l'aide d'algorithmes construisant des solutions tendant vers un optimum\cite{inge}. L'espace de recherche étant infini, l'arrêt du processus d'optimisation est défini par un critère d'arrêt pouvant être par exemple:
    \begin{itemize}
        \item Un seuil d'acceptabilité (réponse approchant l'optimum)
        \item La meilleure solution trouvée après l'écoulement d'un budget d'itérations
    \end{itemize}
    Le champ d'application des problèmes d'optimisation continue est très vaste.
    De manière non exhaustive, on peut citer comme domaine d'application:
    \begin{itemize}
        \item l'étalonnage de modèles biologiques~\cite{dubois:hal-02406612}
        \item l'ingénierie~\cite{LIAO20101188}
        \item la finance~\cite{lozano2011editorial}
    \end{itemize}

    \subsection{Optimisation hors ligne}
    \label{section:offline}
    L'optimisation hors ligne est un sous-ensemble de l'optimisation. Dans ce 
    domaine les algorithmes d'optimisation ont une connaissance complète de 
    l'information sur laquelle ils se reposent pour effectuer la tâche d'optimisation. Aucune nouvelle information ou observation postérieure à la base des connaissances actuelles ne doit pouvoir, en théorie, dégrader ou remettre en cause le(s) solution(s)/ paramétrage(s) déjà déterminées durant le processus d'optimisation. Ce type d'optimisation est pertinent, par exemple, lorsque l'optimisation des paramètres d'un modèle nécessite de nombreuses simulations. C'est notamment le cas des méthodes de "back-analyse"~\cite{JIA201587, KAMRAN2010409, SALIM201526}. En effet, l'algorithme a accès à toutes les informations ainsi que les potentielles informations futures et cherche ainsi le meilleur paramétrage d'un modèle permettant d'expliquer les informations à sa disposition. L'un des objectifs de l'optimisation hors ligne est d'étalonner des modèles n'ayant pas besoin de se corriger rapidement (un nouveau processus d'optimisation pouvant s'opérer après une période d'accumulation de nouvelles informations afin de renforcer le paramétrage du modèle). Les méthodes d'apprentissage supervisé (section~\ref{section:supervised}) peuvent être assimilées à de l'optimisation hors ligne puisque le modèle généré a été appris en amont sur des données sur lesquelles il a toute l'information (caractéristiques et label). 

    \subsection{Optimisation dynamique}
    À l'opposé de cette approche se trouve l'optimisation dynamique ou en ligne. Cette approche part du postulat que l'algorithme n'ait qu'une connaissance partielle ou incomplète du système étudié. En effet, si le système étudié risque d'être (souvent) modifié par des événements inconnus, par exemple si la demande en énergie d'un pays est fortement affectée par un confinement général, alors le modèle doit pouvoir prendre des décisions à la volée faites sur les informations récoltées jusqu'à présent potentiellement combinées avec des hypothèses faites ou des connaissances partielles du futur (prévisions). On peut citer comme exemples, les modèles de contrôle prédictif~\cite{5153127, 6882832}. Dans ce cas l'optimisation ne doit pas excéder l'ordre de la minute~\cite{5153127}. Les modèles de locomotion des robots utilisent aussi ces algorithmes~\cite{crespi2008online, feng20133d}. L'apprentissage par renforcement (section~\ref{section:ml}) peut être assimilé à de l'apprentissage dynamique. En effet le modèle généré doit prendre une décision à partir d'informations accumulées et de connaissances partielles voir incomplètes pouvant être remises en question par des événements inconnus. 

    Ces 2 approches de l'optimisation sont complémentaires et répondent à des 
    besoins spécifiques.

    \subsection{Dérivabilité de la fonction objectif}
    \label{section:convergence}
    %Lorsque la fonction objectif 
    Une fonction est dérivable en un point $a$ quand elle admet une dérivée finie en $a$, c'est-à-dire, quand elle peut être approchée de manière assez fine par une fonction affine au voisinage de $a$. Elle est dérivable sur un intervalle réel ouvert non vide si elle est dérivable en chaque point de cet intervalle. Plus formellement soit, $I$ un interval non vide de $\mathbb{R}$, $f: \rightarrow \mathbb{R}$  et $x_0 \in I$, on dit que $f$ est dérivable en $x_0$ si  $ f'(x_0) = \lim_{h \to x_0} \frac{f(x_0 + h) - f(x_0) }{h}$ est finie. Cette limite est appelée la dérivée de $f$.
    
    Lorsque la fonction objective d'un problème d'optimisation est dérivable
    ce qui peut être possible dans le cadre de l'optimisation numérique et combinatoire, alors l'algorithme peut tirer parti de la connaissance du gradient (on parle alors de \emph{gradient-based method }) pour trouver des solutions de meilleure qualité par rapport à la fonction de coût. En effet si l'on considère le cas d'une fonction de coût cherchant à minimiser l'erreur, un algorithme basé sur l'utilisation du gradient consiste  à partir d'un point de départ (solution candidate) appartenant à l'espace de recherche de la fonction de coût, translater ce point dans la direction opposée au gradient afin de diminuer l'erreur et ainsi atteindre une solution de meilleure qualité à chaque itération sous réserve que le pas soit suffisamment petit. Cette technique est appelée descente de gradient. L'atout principal de cette technique est la possibilité d'optimiser de nombreux paramètres de la fonction objectif et donc de résoudre des problèmes de grande dimension tout en ayant recours à peu d'évaluations~\cite{venter2010review}. À noter qu'il existe d'autres méthodes d'optimisation de manière non exhaustive, on peut citer: adam, nadam, nesterov~\cite{ruder2016overview}. Malheureusement, l'avantage de la descente de gradient est aussi sa principale faiblesse. Lorsque la fonction à optimiser est multimodale, non dérivable, non convexe, \dots  (figure~\ref{fig:fctmulimodal}) la convergence vers une solution toujours de meilleures qualités peut l'obliger à converger vers une solution sous optimale (appelé optimum local). Il faut alors utiliser des techniques stochastiques (faisant appel à l'aléatoire) pouvant remédier à ce problème~\cite{bottou2012stochastic}. Les méta heuristiques (section~\ref{section:metahe}) sont de bonnes candidates pour résoudre ces problèmes. Grâce à elles de nombreux problèmes issus du monde réel utilisant des fonctions de coût non dérivables, peuvent ainsi trouver une solution approximant la ou les solutions optimales. De manière non exhaustive, on peut citer le domaine de la chimie~\cite{deming1978review},  biologie~\cite{gray2004optimizing}, de la médecine~\cite{marsden2008computational, oeuvray2005trust}, de  l'ingénierie~\cite{han2008optimal}. 
    \begin{figure}
        \includegraphics[scale=0.25]{figures/spheref.png}
        \includegraphics[scale=0.25]{figures/func_2D_icop.eps}
        \caption{Fonctions convexes lisse (à gauche) et bruitée (à droite).}
        \label{fig:optimalocaux}
    \end{figure}
    Comme il existe de très nombreux algorithmes d'optimisation, il est 
    nécessaire de choisir le mieux adapté. Le critère principal pour le choix d'un algorithme dans la résolution d'un problème d'optimisation est le nombre d'évaluations nécessaires pour arriver à converger vers la solution optimale.

    \subsection{Connaissance analytique du problème}
    Quand la définition analytique de la fonction de coût est connue alors 
    on parle d'optimisation \emph{boîte blanche}. Dans ce contexte, la 
    définition  devient alors une information supplémentaire pour 
    l'algorithme, il est alors possible d'utiliser ses caractéristiques 
    algébriques afin de le résoudre plus efficacement (descente de gradient \dots). L'optimisation \emph{boîte noire} quant à elle, désigne toutes les fonctions objectives qui n'ont pas accès à cette définition formelle ou bien lorsque l'algorithme d'optimisation n'a pas nécessairement besoin de la connaître pour la résoudre.
    Ainsi la qualité d'une solution $x$  n'est accessible qu'à travers la 
    réponse d'un oracle $f(x)$. Ce dernier pouvant représenter le résultat 
    d'une simulation, ou d'un calcul. L'algorithme n'a donc que la possibilité de connaître. ${(x_0, f(x_0), (x_1, f(x_1))\dots}$
    
    %Le choix d'un algorithme d'optimisation adapté au problème rencontré relève 
    %soit d'une étude empirique, soit d'une utilisation de méthode de sélection de portefeuille d'algorithmes~\cite{MUNOZ2015224} 

    \subsection{Fonction multimodale}
    \label{section:multimodal}
    Lorsqu'une fonction n'est pas convexe, elle possède un ou plusieurs 
    optima locaux. Ces solutions correspondent à une qualité de réponse 
    localement meilleure que les autres, mais potentiellement sous-optimale par rapport à la solution optimale (appelée aussi globale) (voir la figure \ref{fig:optimalocaux}). On parle de fonction multimodale dans 2 cas de figure:
    \begin{itemize}
     \item La fonction possède un optimum global, mais de nombreux optima locaux (dont certains peuvent avoir une qualité très proche de celle optimale). 
      Dans ce cas il est nécessaire de découvrir le plus grand nombre afin de trouver la meilleure.
     \item la fonction possède plusieurs optima globaux, c'est-à-dire que 
     plusieurs solutions ayant des paramètres plus ou moins différents répondent au mieux aux critères de la fonction objective.
    \end{itemize}
    Les problèmes provenant du monde réel sont souvent multimodaux~\cite{teytaud_2011_qrrs}.

    \begin{figure}
        \centering
        \includegraphics[scale=0.42]{figures/multimodale.png}
        \caption{Schéma illustrant une fonction avec des optima locaux et un 
        optimum global en minimisation. }
        \label{fig:fctmulimodal}
      \end{figure} 
    
  \section{Méta heuristique}
   \label{section:metahe}
  Une heuristique est un algorithme de  résolution dont la conception repose sur l'expérience de son concepteur. Une heuristique est un compromis entre une performance efficace et une possibilité d'utilisation. En effet, elle ne 
  garantit pas d'obtenir une réponse optimale à un problème, cependant elle doit pouvoir le plus souvent fournir une réponse approximant la solution optimale tout en fournissant le moins souvent une solution de piètre 
  qualité~\footnote{Il faut comprendre ici que l'heuristique doit être le plus 
  généralisable possible.} et enfin, être facile à implémenter~\footnote{Dans sa 
  version initiale tous du moins}. De plus leur temps d'exécution doivent être 
  raisonnable. Puisqu'il est peu probable qu'une seule heuristique réponde à tous les problèmes d'optimisation, une combinaison de plusieurs heuristiques appelées méta heuristique a été proposée. Ces algorithmes peuvent ainsi résoudre des problèmes difficiles pour une simple heuristique. C'est le cas des optimisations continues boîte noire et/ou multimodale.

  On distingue 2 grandes classes de méta heuristique: les algorithmes gérant 1 
  solution et les algorithmes gérant une population de solutions. Le reste de la section présente les différentes méta heuristiques utilisées durant cette thèse. 
  
\subsection{Stratégies d'évolution} 
Ces algorithmes sont inspirés de la théorie darwinienne de l'évolution.
Selon ce principe, la pression exercée par un milieu permet aux solutions les 
plus adaptées de pouvoir survivre et de ce fait se reproduire. Dans son implémentation pour les algorithmes d'optimisation, la fonction objective sert d'environnement. La survie des solutions les mieux adaptées est déterminée par un opérateur de sélection. Enfin la reproduction est effectuée sur les solutions susceptibles de se reproduire par une méthode de variation aléatoire qui peut comprendre une étape dite de mutation et une étape dite de croisement.
Les \gls{es} sont des méthodes stochastiques, puisqu'il y a au 
moins un de ses opérateurs qui utilise des processus aléatoires.
Le fonctionnement peut être résumé ainsi:
à partir d'une population de solutions, généralement tirée aléatoirement, un 
ensemble des meilleures (mieux adaptés) solutions est sélectionné.
Ces solutions seront les parents de la nouvelle génération de la population 
grâce à l'opérateur de mutation (ou croisement). La notation générale des stratégies d'évolution est $(\mu+\lambda)$-ES avec $\mu$ parents et $\lambda$ enfants. Le remplacement des $\mu$ meilleurs est effectué parmi les $\mu + \lambda$ solutions. Il existe un cas particulier où la taille de la population est de 1 (noté (1+1)-ES) ce qui signifie que le meilleur individu entre le parent et son enfant est conservé.
  
Itérativement, et en accord avec le principe darwinien, l'ensemble de la 
population va tendre à devenir plus adapté aux critères de la fonction 
objective. L'algorithme~\ref{algo:es} schématise le principe général régissant les algorithmes évolutionnaires. Les travaux de cette thèse se sont centrés sur l'étude des stratégies d'évolution pour répondre au problème de calibration du modèle WEEDRIQ dans un contexte boîte noire nous interdisant la connaissance du gradient. Le reste de la section présente 2 algorithmes état de l'art utilisés durant la thèse dans un contexte continu multimodal boîte noire. Le premier est un algorithme à population unique tandis que le second est un algorithme à population.

\begin{algorithm}
    \caption{ Algorithme de la stratégie d'évolution}
    \label{algo:es}
    \DontPrintSemicolon
   
    \Begin{
        $P \gets initialisation\_aleratoire()$ \;
        \While {critere d'arret non satisfait} {
            $parents \gets \text{SelectionMeilleur}(P)$ \;
            $enfants \gets mutation(parents)$ \;
            $P \gets remplacer (P \cup enfants)$\;
        }
    }
\end{algorithm}

\subsubsection{(Quasi) Random with Decreasing Step size}
   \label{section:es}
Dans le contexte des problèmes d'optimisation multimodale boîte noire, la 
fonction objectif possède beaucoup d'optima globaux ou bien de nombreux 
optima locaux proches de l'optimum global. Le but consiste donc  à découvrir le plus d'optima possible afin de sélectionner le ou les meilleurs a posteriori. Du fait de l'hypothèse de la boîte noire, l'utilisation de techniques basées sur le gradient est impossible. Les algorithmes \gls{rds}~\cite{teytaud_2011_qrrs} et \gls{qrds}~\cite{teytaud2016qr} ont été proposés afin de répondre à ce problème. Ce sont des algorithmes d'optimisation proposés pour répondre aux problèmes d'optimisation multimodale boîte noire. Ils utilisent le principe de la recherche locale à solution unique contrôlée par une stratégie de relance ou  \emph{restart}. La recherche locale peut être effectuée avec n'importe quel algorithme à stratégie d'évolution~\cite{back1991survey}. Dans sa version initiale, l'algorithme utilisé est un (1+1)-ES avec le step-size respectant la règle du "one-fifth rule success"~\cite{auger2009benchmarking}.  Cette règle implémente l'idée que le step-size doit augmenter si les sélections de la recherche locale se font sur les solutions candidates par rapport à la solution courante. Ceci suggère que la taille du voisinage est trop petite et doit donc être augmentée. Inversement, si la solution courante est plus souvent gardée que la solution candidate, alors l'échantillonnage des solutions candidates est trop large. Il faut donc réduire le step-size. De manière optimale, la probabilité d'échantillonner correctement une solution meilleure tend vers 1/5~\cite{rechenberg_1973_es, schumer1968adaptive}.

L'algorithme~\ref{alg:searchds} présente le principe de fonctionnement de la recherche locale SearchDS (utilisé dans les algorithmes RDS et QRDS). Cet algorithme fait évoluer itérativement les solutions candidates: il génère une nouvelle solution par mutation  en utilisant une distribution normale avec un écart-type $\sigma$. Si le nouveau point est meilleur alors la valeur de $\sigma$ est augmentée, car cela signifie que le point actuel est loin de la solution, ainsi la recherche doit être étendue à un voisinage plus grand. Dans le cas contraire, $\sigma$ est diminué parce que cela signifie que le point actuel est proche de la solution et donc la recherche doit se focaliser sur un voisinage restreint. La meilleure solution (par rapport à la fonction de coût) entre le nouveau et l'ancien est conservée. La recherche est terminée si la solution converge vers les coordonnées d'un optimum déjà découvert ou bien lorsque la recherche converge à une distance $\epsilon$ d’un optimum (dans ce cas, les coordonnées de cette solution ou le point sont mémorisés). En utilisant cet algorithme de recherche local, une stratégie de restart est très simple à implémenter, comme le montre l'algorithme~\ref{alg:qrds}. À chaque itération, un point est sélectionné à travers l'espace de recherche. Ce dernier est utilisé comme point de départ d'une recherche locale. Quand la recherche locale est terminée, une nouvelle  recherche locale commence si tous les optima n'ont pas été découverts et si le budget d'évaluation n'est pas écoulé. Schoenauer et al.~\cite{teytaud_2011_qrrs} ont comparé les 2 algorithmes: \gls{rds} et \gls{qrds}. Ils ont démontré que l'échantillonnage du point de départ suivant une séquence quasi aléatoire (QRDS) plutôt qu'une loi uniforme (RDS) était plus efficace. Une caractéristique importante de QRDS est le "murder operator". Dans le but d'éviter de converger sur des optima déjà découverts, l'algorithme vérifie à chaque évaluation si la solution courante est à une distance (euclidienne) supérieure à un seuil $\epsilon_x$ de chaque optimum déjà mémorisé et donc découvert. Si c'est le cas, la recherche est arrêtée prématurément sans sauvegarder la solution (l'idée est de ne pas gaspiller le budget d'évaluation pour des optima déjà découverts).

    \begin{algorithm}
        \DontPrintSemicolon    
        \;
    
        \KwIn{~
            \\~~$f$: fonction a optimiser
            \\~~$\sigma_0$: step-size
            \\~~$\epsilon_\sigma$: seuil du step-size
            \\~~$y^*$: valeur maximale de la fonction objectif
            \\~~$\epsilon_y$: seuil de la valeur de la fitness
            \\~~$\mathbf{x}$: point de départ de la recherche
            \\~~$\epsilon_x$: seuil de la valeur de la position de la recherche
            \\~~$\mathbf{\hat{X}}$: ensemble des optima déjà découvert
        }
    
        \KwOut{~
            \\~~$\mathbf{\hat{X}}$: ensemble mise à jour des optima locaux
            \\~~$y$: valeur de l'optimum
        }
    
        \Begin{
            $y \gets f(\mathbf{x})$ \;
            $\sigma \gets \sigma_0$ \;
            \Repeat {$\sigma < \epsilon_\sigma$} {
                \# mutation \;
                $\mathbf{x}' \gets \mathcal{N}(\mathbf{x}, \sigma)$ \;
                $y' \gets f(\mathbf{x}')$ \;
                \# selection selon le $1/5$e adaptation \;
                \If {$y' < y$} {
                    $\mathbf{x} \gets \mathbf{x}'$ \;
                    $\sigma \gets 2 \sigma$ \;
                } 
                \Else {
                    $\sigma \gets 2^{-1/4} \sigma$ \;
                }
                \# arreter la recherche si déjà connue \;
                \If {$\exists \mathbf{\hat{x}} \in \mathbf{\hat{X}}, \lVert 
                \mathbf{x} - \mathbf{\hat{x}} \rVert < \epsilon_x$ } {
                    \textbf{break} \;
                } 
                \# mémoriser l'optimum découvert \;
                \If {$\lVert y - y^* \rVert < \epsilon_y$} {
                    $\mathbf{\hat{X}} \gets\mathbf{\hat{X}}\cup\{\mathbf{x}\}$\;
                    \textbf{break} \;
                } 
            }
        }
    
        \caption{\label{alg:searchds}Recherche d'un optimum en utilisant un step size décroissant, SearchDS (minimisation) }
    \end{algorithm}
    
    \begin{algorithm}
        \DontPrintSemicolon
    
        \KwIn{
            $f, \sigma_0, \epsilon_\sigma, y^*, \epsilon_y, \mathbf{x},
             \epsilon_x$
        }
    
        \KwOut{
            $\mathbf{\hat{X}}$
        }
    
        \Begin{
            $\mathbf{\hat{X}} \gets \varnothing$ \;
            \While {tous les optima n'ont pas été découverts} {
                $\mathbf{x} \gets \text{SampleQR}()$ \;
                $\mathbf{\hat{X}}, y \gets \text{SearchDS}(f, \sigma_0, 
                \epsilon_\sigma, y^*, \epsilon_y, \mathbf{x}, \epsilon_x, 
                \mathbf{\hat{X}})$ \;
            }
        }
    
        \caption{\label{alg:qrds} Quasi-random Restarts with Decreasing Step-size }
    \end{algorithm}
\subsubsection{Covariance Matrix Adaptation Evolution Strategy}
    CMA-ES~\cite{cmaes} (signifiant stratégie d’évolution à matrice de covariance adaptative) est une stratégie d'évolution pour l'optimisation continue à destination des problèmes non linéaires ou non convexes.
    Contrairement aux méthodes à solution unique, CMA-ES utilise une population 
    de solutions, ainsi le risque de convergence prématurée sur des optima locaux de CMA-ES est réduit si la taille de la population est suffisamment grande~\cite{van2000cooperative}. CMA-ES suppose la distribution de la population de solution comme une loi normale multivariée dont la matrice de covariance est un paramètre appris au cours de l’optimisation à l'aide d'une distribution de recherche normale\cite{hansen2003reducing}. L'algorithme~\ref{algo:cmaes} présente le principe de fonctionnement de l'algorithme. Une propriété importante de cet algorithme est son invariance par transformations linéaires de l'espace de recherche. CMA-ES est efficace pour trouver la solution de fonction unimodale et est supérieure à d'autres algorithmes état de l'art lorsque le problème est mal posé et non séparable.
    La figure~\ref{fig:cmae_graph} schématise itérativement le processus de 
    convergence de CMA-ES. Itérativement l'algorithme génère des solutions 
    candidates à partir de la moyenne des $\mu$ meilleures solutions courantes dans un voisinage $\sigma$ et selon une loi normale définie dont les paramètres $C$ sont donnés par la matrice de covariance. La sélection des meilleures solutions candidates permet de mettre à jour le vecteur moyen ainsi que la matrice de covariance et enfin d'adapter la taille du pas de la recherche. L'opération est répétée jusqu'à ce que la recherche converge ou qu'un critère d'arrêt ne soit pas atteint.    
    
    \begin{figure}
        \centering
        \includegraphics[scale=0.85]{figures/cmaes_algo}
        \caption{Schéma illustrant la convergence de CMA-ES. La couleur blanche 
        indique la zone optimale où converger, les cercles représentent les 
        différentes lignes d'iso fitness.  (source Wikipédia).}
        \label{fig:cmae_graph}
      \end{figure}  

    \subsubsection{Covariance Matrix Adaptation Evolution Strategy with Increasing POPulation}
    Dans le contexte des problèmes d'optimisation multimodale, \cite{hansen2004evaluating} montre que l'augmentation de la taille de la population peut améliorer les performances de CMA-ES. Auger et al.~\cite{Auger:2005} propose une version de CMA-ES utilisant une stratégie de redémarrage:
    à chaque redémarrage (chaque fois que le critère d'arrêt est rempli) 
    la taille de la population est doublée comme expliqué par 
    l'algorithme~\ref{algo:ipop}.
    En augmentant la taille de la population, la recherche locale devient alors 
    de plus en plus globale après chaque redémarrage. Les résultats donnés dans 
    \cite{Auger:2005} montrent que cette amélioration apporte une bonne 
    performance dans le contexte d'une boîte noire multimodale.
    L'algorithme~\ref{algo:ipop} présente le principe de fonctionnement de 
    CMA-ES IPOP afin de gérer les problèmes multimodaux.
    
    \begin{algorithm}
        \DontPrintSemicolon
        
        \KwIn{\\~~$f$: fonction à optimiser\\~~$\lambda$:  nombre d'échantillons 
        par itération \\~~$x$: vecteur de population\\~~$s$: vecteur des 
        fitness}
         
        \KwOut{$x_1$: best optima find so far}
        
        \Begin{ \While{stop when criterion  met}
         {\For{i in
            {1\dots$\lambda$}} {$x_i \gets \mathcal{N}$($\mu, C, \sigma$) \; $s_i \gets $ $f(i)$	\;
            }
            Sort($x_{1\dots \lambda}$ )\; Update\_mean\_to\_better\_solution($\mu, C, \sigma$)\;
            Update\_isotropic\_evolution\_path($\mu, C, \sigma$)\;
            Update\_anisotropic\_evolution\_path($\mu, C, \sigma$)\; Update\_covariance\_matrix($\mu, C, \sigma$)\;
            Update\_step-size($\mu, C, \sigma$)\;
            }}
        \caption{\label{algo:cmaes} Covariance Matrix Adaptation Evolution Strategy (CMA-ES)}
    \end{algorithm}
    
    \begin{algorithm}
        \DontPrintSemicolon
        \KwIn{\\~~$f$: function to optimize \\~~$\lambda$: number of sample per
            iteration \\~~$\lambda_0$: size of the initial population \\~~$n$:
            number of restart \\~~$X$: set of solution}\KwOut{$X$ or  $ X_1$:set
            of optima or the best one} \Begin{$n \leftarrow 0$ \While{number of
            evaluations not reach} {$n \leftarrow n + 1$\; $\lambda \leftarrow
            \lambda_0 + 2 \times n$\; $X \leftarrow$ cmaes($\lambda, f$)\;}}
        \caption{\label{algo:ipop}Covariance Matrix Adaptation Evolution Strategy with Increasing POPulation CMA-ES-IPOP}
    \end{algorithm}

\subsubsection{Compromis exploration/exploitation}
À l'origine du développement et du choix de design de n'importe quel 
algorithme d'optimisation se trouve la question du compromis exploration/exploitation. En effet jusqu'ici la présentation du problème d'optimisation n'a mis en lumière que la partie exploitation. Cependant, comme expliqué dans la section~\ref{section:convergence}, les algorithmes peuvent converger prématurément vers des optima locaux. De ce fait avoir la possibilité d'explorer l'espace de recherche est intéressant afin d'éviter ce comportement. Le compromis apparaît donc comme suit: un algorithme privilégiant une exploration ne pourra converger tandis qu'un algorithme privilégiant l'exploitation risque de converger sur un optimum local. Les algorithmes doivent donc prendre en compte ce compromis afin de proposer une architecture capable de traiter avec ce compromis.

\paragraph{Synthèse}
Ce chapitre orienté sur les problèmes d'optimisation se divise en trois parties. La première partie, après une courte introduction, donne une définition d'un problème d'optimisation. La deuxième partie présente différentes typologies et caractéristiques intrinsèques des problèmes d'optimisation. Ce point est important à prendre en compte lors du choix de la méthode de résolution du problème. La dernière partie aborde la résolution des problèmes d'optimisation en se concentrant sur les problèmes d'optimisation numérique continue mono-objectif dans un contexte boîte noire. L'intérêt des méta heuristiques comme méthode de résolution y est mis en avant. S'ensuit une présentation détaillée issue de la littérature d'un échantillon d'algorithmes d'optimisation continue mono-objectif boîte noire multimodale. Ces derniers ont été choisis puisque la classe des problèmes d'optimisations traités dans la suite du manuscrit sont continue, multimodale boîte noire. Les algorithmes RDS et QRDS utilisent une population à solution unique tandis que les algorithmes CMAES et une de ses variantes, CMAES-IPOP, maintiennent une population de plusieurs solutions. Ce chapitre pose les bases des travaux en optimisation qui seront utilisés dans les chapitres suivants.
