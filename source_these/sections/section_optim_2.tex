\chapter{Calibration de modèles paramétriques}
\paragraph{Introduction} Ce chapitre présente une contribution scientifique.
 Le problème de calibration y est présenté comme un problème d'optimisation numérique boîte noire. La contribution majeure~\cite{dubois:hal-02406612} propose une nouvelle méthodologie d'étalonnage automatique des paramètres de modèles biologiques grâce à l'utilisation de l'analyse inverse~\cite{CALVELLO2004410}. Les travaux portent en premier lieu sur une comparaison de performance entre les jeux de paramètres optimisés et celui des experts dans l'estimation de la teneur en eau dans le sol au cours d'une campagne. La performance d'un jeu de paramètres correspond à l'erreur de simulation avec ce paramétrage par rapport à des valeurs étalons. S'ensuit une comparaison de deux algorithmes état de l'art (2019) pour ce type de problème utilisant des stratégies d'évolution différentes. Le critère de comparaison utilisé est la fitness associée à un jeu de paramètre découvert après qu'un budget d'évaluations soit écoulé. Enfin la fiabilité et le non-surapprentissage d'un jeu de paramètres optimisés du modèle WEEDRIQ par rapport au paramétrage expert sont aussi à l'étude via une validation croisée sur les différentes instances de test. L'expérience est effectuée avec le modèle de bilan hydrique WEEDRIQ~\cite{Ramat:2002}. Les performances de la méthodologie sont établies au travers de différentes instances dont les données sont issues de mesures réelles de campagne de cultures historiques.

\section{Étalonnage automatique de modèle paramétrique}
\label{section:contribution1} 
\subsection{Contexte}
Comme expliqué dans la section~\ref{section:intro_capteurs} de la partie introduction, en agriculture, de nouvelles techniques de prise de décision font appel à des capteurs hydriques pour mesurer l'état des plantes et du sol afin d'estimer la teneur en eau du sol. Couplé à un modèle de croissance de plante il est possible d'estimer l'état hydrique de la plante ainsi que la quantité d'eau disponible pour celle-ci, car dépendant de la taille (profondeur) des racines et des caractéristiques du sol. Bien qu'une telle approche numérique puisse permettre d'estimer avec précision l'état de la culture et que la précision des capteurs soit bonne, un inconvénient  majeur est le réglage des (très) nombreux paramètres des  modèles agronomiques. En effet, les modèles combinent souvent différents sous-modèles basés sur des équations différentielles, des transitions d'états finis, etc~\dots Ceci requiert le réglage des paramètres numériques, biologiques ou bien géologiques. Même si les experts peuvent mesurer, estimer ou donner des bornes à certaines valeurs de paramètres\footnote{Certains paramètres numériques peuvent n’avoir aucune signification réelle ou biologique et donc n'ont en théories aucune borne}, la plupart  du temps, leur valeur n'est pas connue pour une culture de plein champ, car dépendante d'un sol spécifique et d'une espèce/variété de plantes. 
Cette contribution  explique comment il est possible de définir plus précisément les paramètres du modèle de croissance des plants de pommes de 
terre WEEDRIQ~\cite{Ramat:2002} grâce à l'acquisition de données des capteurs 
hydriques (fournis par la société \emph{WEENAT}) et d'un algorithme d'optimisation pertinent qui minimise l'erreur entre les valeurs prédites (calculées par le modèle), et celles fournies par les capteurs.
WEEDRIQ étant une collection de modèles biologiques dont la définition 
analytique des équations est complexe, elle peut être vue comme une boîte 
noire. Dans le domaine évolutionnaire, ce problème boîte noire peut être vu 
comme un problème d'étalonnage ("calibration problem")~\cite{tang2006effective, 
 gupta2006model}. Le paramétrage du modèle de croissance du plant de pommes de terre montre des difficultés spécifiques. Ses paramètres dépendent des spécificités locales telles que le sol, la variété de pommes de terre, l'exposition aux intempéries, etc.~\dots De plus les données sont difficiles à recueillir et rares. En effet, une campagne d'acquisition de données avec des capteurs hydriques pour un champ de pommes de terre dure en moyenne 4 mois, et ne peut être effectuée sur le même champ que tous les 5 ans (et parfois 3 ans) en raison de la rotation des cultures. En outre, le nombre de paramètres des modèles est élevé : plusieurs dizaines pour des modèles représentatifs tels que STICS~\cite{whisler1986crop}, AquaCrop~\cite{raes2009aquacrop} ou Weedric~\cite{Ramat:2002}.

Au début de la thèse, ce problème avait été traité comme un problème mono modal. Cependant, chaque résultat d'exécution d'algorithme d'optimisation (en utilisant l'algorithme CMA-ES) ne donne pas, à un delta près, des solutions proches (en considérant la distance euclidienne) les unes des autres bien que les fitness associées soient proches. L'hypothèse forte de la multimodalité du problème a été posée. %La 
%Suite à une étude préliminaire, 
%fonction objectif bien que connu (les équations des modèles étant elle même connu), peut être difficile à exploiter à cause des relations entre les modèles. Ainsi nous traitons le problème comme un e  
%Ainsi, le problème d'optimisation lié à l'étalonnage apparait se présenter comme un problème ayant beaucoup d'optima locaux 

%n'est pas seulement un 
%problème avec  mais aussi un problème multimodal 
%dans lequel la qualité de nombreux optima est très proche de celle de 
%l'optimum global.


\subsection{Travaux similaires}
  Le problème lié à l'étalonnage automatique de paramètres de modèle est 
  souvent traité en utilisant la méthode d'analyse inverse~\cite{CALVELLO2004410}. %L'idée consiste à étalonner itérativement un 
  %modèle en changeant les valeurs d'entrée jusqu'à ce que les valeurs de 
  %sortie correspondent à des données observables (valeurs étalon).
  %Ces méthodes utilisent les techniques de l'optimisation hors 
  %ligne (section~\ref{section:offline}). 
  La figure~\ref{fig:inverse} schématise le principe de l'analyse inverse.  À partir d'un jeu de paramètres initiaux, itérativement, un jeu de paramètres est passé au modèle paramétrique. L'erreur entre les valeurs de sorties (prédiction) et des observations (valeurs étalons) est mesurée par une métrique (fonction objective). Si un critère d'arrêt n'est pas atteint alors l'erreur est transmise à l'algorithme d'optimisation. Cette erreur correspond à la fitness associée à la solution courante (jeu de paramètres). L'algorithme d'optimisation détermine alors un nouveau jeu de paramètres dans l'espace de recherche. Ce processus est itéré jusqu'à ce que les valeurs de sortie correspondent à des données observables (valeurs étalons).

  \begin{figure}
    \centering
    \includegraphics[scale=0.250]{figures/back_analyse.png}
    \caption{Schéma du principe de l'analyse inverse.}
    \label{fig:inverse}
  \end{figure}  

  Concernant les problèmes d'optimisation multimodale ou \gls{mmo} en anglais, 
  de nombreux algorithmes ont été proposés, certains utilisant la dérivabilité 
  du gradient (voir section~\ref{section:convergence}). Malheureusement, dans un contexte boîte noire, ces algorithmes ne sont pas directement applicables. Les autres méthodes, n'utilisant pas le gradient (gradient free), utilisent   généralement les stratégies évolutionnaires (\gls{es}). Ces dernières ont montré leur robustesse et leur efficacité dans la résolution de problème académique d'optimisation continue mono-objectif boîte noire~\cite{beyer2001es,nevergrad,rechenberg_1973_es}. Les \gls{es} consistent à trouver itérativement de meilleures solutions à partir d'un point de départ. Dans le contexte des problèmes d'optimisation multimodaux, les stratégies évolutionnaires sont généralement combinées avec soit des techniques de "niching" soit des "restart strategy" afin de trouver tous les optima. 
  

\subsection{Définition du problème d'optimisation}

Le problème d'optimisation de l'étalonnage du modèle d'irrigation 
WEEDRIQ~\ref{section:weedriq} en utilisant les observations des données récoltées par les capteurs peut être défini comme suit. Le modèle possède des paramètres ($d=36$ pour WEEDRIQ) réels pour lesquels aucune connaissance a priori sur leur implication dans les équations, simulations ou interactions dans les différents modèles n'est disponible. La borne de chaque paramètre est donnée par des experts (valeur de la FAO). Ainsi on peut le considérer comme une fonction boîte noire avec un espace de dimension $d > 1 : [0, 1]^d$, la valeur de chaque paramètre est normalisée pour éviter l'effet d'échelle. Le processus de normalisation est effectué grâce à l'équation~\ref{eq:norm} où $x$ est un paramètre du modèle WEEDRIQ. 
\begin{equation}
    x_{norm} = \frac{x - x_{min}}{ x_{max} - x_{min}}
    \label{eq:norm}
\end{equation}
Comme expliqué précédemment, l'hypothèse de la forte multimodalité du problème  avec énormément d'optima locaux proches de l'optimum est posée. Il est donc nécessaire de trouver le maximum d'optima afin de découvrir le meilleur. Les capteurs étant en mesure de collecter les informations sur la teneur en eau présente dans le sol (par l'intermédiaire de la transformation du potentiel hydrique par l'équation de Van Genuchten~\cite{van1980closed}), leurs valeurs permettent d’évaluer l’erreur de la simulation du modèle. On peut donc définir l'erreur comme étant la somme au carré des distances entre les valeurs simulées et les valeurs capteurs, ceci étant résumé par l'équation~\ref{eq:swc_mse}, où $x$ correspond à un vecteur de solution tel que $ x = ({x_1, \dots, x_{36}})$, $n$ le nombre de jours de simulation, $SWC_t$ la valeur capteur à l'instant $t$ transformée par l'équation de Van Genuchten en teneur en eau et $SWC\_TNT_t(x)$ la teneur en eau estimée à l'instant $t$ par le modèle WEEDRIQ en utilisant le paramétrage $x$. 
\begin{equation}
     f(x) = \sqrt{ \sum\limits_{t=1}^n{\frac{1}{n} (SWC_t - SWC\_TNT_t(x))^2}}
     \label{eq:swc_mse}    
\end{equation}

\subsection{Protocole expérimental}
Initialement, le modèle WEEDRIQ possède des valeurs de paramètres par défaut établis par des experts (valeurs de la FAO). L'étude porte en premier lieu sur la pertinence de l'étalonnage automatique de WEEDRIQ en utilisant la méthode de l'analyse inverse~\cite{CALVELLO2004410} grâce aux algorithmes d’optimisation par stratégie d’évolution multimodale (voir section \ref{section:es}) par rapport à l'utilisation du paramétrage expert. La méthode de l'analyse inverse postule qu'il est possible d'établir la performance d'un jeu de paramètres d'un modèle paramétrique comme étant une mesure d'écart entre la prédiction du modèle avec ce paramétrage et des valeurs étalons. On remarque qu'un problème de minimisation apparaît puisqu'il faut effectivement diminuer cette erreur pour obtenir un jeu de paramètres pertinent. L'optimisation se fera hors ligne, puisqu'il est nécessaire de connaître toutes les valeurs capteurs afin d'utiliser l'analyse inverse. D'autre part cette étude porte aussi sur une comparaison de performance entre 2 algorithmes ayant des stratégies différentes:
\begin{itemize}
    \item QRDS,  algorithme d'optimisation à solution unique combinée 
    avec une stratégie de redémarrage. L'intérêt réside en sa convergence rapide et sa capacité d'exploration de l'espace de recherche grâce au séquençage du point de départ suivant une suite de nombres quasi aléatoires (suite de sobol).
    \item CMAES-IPOP, est une variante de CMAES pour une utilisation dans un contexte \gls{mmo}. Sa matrice de covariance et l'augmentation de la population à chaque redémarrage doivent empêcher l'algorithme de rester bloqué dans des optima locaux.
\end{itemize}

\subsection{Expériences et résultats}
Les données qui ont pu être récoltées correspondent à 5 campagnes de cultures.
Ces campagnes ont toutes eu lieu dans des zones géographiques proches et selon les experts avec un type de sol ne variant pas ou peu. Seule la variété de pommes de terre cultivée est différente. Comme l'optimisation doit pouvoir être exécutée en temps raisonnable en vue d'une mise en production, le budget alloué à chaque algorithme est d’ $10^5$ évaluations (le temps d'exécution est de l'ordre de la dixième de seconde). Afin de pouvoir comparer les performances sur la quantité peu élevée de campagnes de culture, 100 optimisations sont réalisées pour chaque algorithme sur chaque champ. Les paramètres de l'algorithme QRDS sont de $10^{-4}$ pour $\epsilon_\sigma$, $0.5$ pour $\epsilon_x$ et $5*10^{-3}$ pour $\epsilon_y$.  le paramétrage  de CMAES-IPOP est celui par défaut.

\begin{figure}
    \centering
    \includegraphics[scale=0.55]{figures/SWC_sim.eps}
    \caption{Simulation de la teneur en eau sur une campagne de culture. Sont 
    représentées en noir les valeurs étalons issues des capteurs, en vert les valeurs du simulateur avec le paramétrage par défaut et en rouge celles avec le paramétrage optimisé.}
    \label{fig:res_exp_optim}
\end{figure}  

\paragraph{Discussion}
 la figure~\ref{fig:res_exp_optim} présente un exemple de campagne de culture parmi les 5 disponibles. La valeur réelle (capteur) est représentée par la courbe noire, celle du simulateur avec le paramétrage par défaut (expert) en vert et celle avec le paramétrage suite à une optimisation en rouge. Visuellement, les résultats indiquent clairement une amélioration de la simulation suite à l'optimisation des paramètres. Numériquement, les valeurs pour chaque champ sont explicitées ci-après.

 \begin{figure}
    \centering
    \includegraphics[scale=0.70]{figures/perf_mmo_crops.pdf}
    \caption{Distribution des meilleurs optima trouvés à chaque optimisation pour chaque champ en fonction de l'algorithme d'optimisation utilisé. Une valeur faible indique une meilleure performance.}
    \label{fig:res_crop_optim}
\end{figure}  
 
\begin{figure}
    \centering
    \includegraphics[scale=0.55]{figures/boxbestEverFitness.eps}
    \caption{Distribution des meilleurs optima trouvés après 100 optimisations 
    pour chaque champ en fonction de l'algorithme utilisé. Les points rouges représentent une valeur en dehors de l'intervalle de confiance. Une valeur faible indique une meilleure performance.}
    \label{fig:res_box_plot_optim}
\end{figure} 



    \begin{table}
        \caption{Performances des jeux de paramètres pour la prédiction de l'évolution de la teneur en eau dans le sol en fonction de la méthode utilisée et de la parcelle de test. Les valeurs médianes en gras sont significativement meilleures selon le test Mann-Whitney à un niveau de confiance de $0,01 $.
        QRDS trouve toujours le meilleur jeu de paramètres, de plus la moyenne
        et la médiane des fitness sont meilleures (plus petite).}
        \label{tab:results}
            \begin{tabular}{llccc}
                \toprule
                \textbf{Crop} & \textbf{Algorithm} & \textbf{Best} & \textbf{Mean} &
                \textbf{Median} \\ 
                \midrule
                \multirow{3}{*}{Crop 1} 
                & \multicolumn{1}{l}{Default} & \multicolumn{1}{c}{80.83} &
                \multicolumn{1}{c}{ / } & \multicolumn{1}{c}{ / } \\
                & \multicolumn{1}{l}{QRDS} & \multicolumn{1}{c}{\textbf{15.8}} &
                \multicolumn{1}{c}{$16.3 \pm 0.03 $} & \multicolumn{1}{c}{ \textbf{16.3} }
                \\
                & \multicolumn{1}{l}{CMA-ES IPOP} & \multicolumn{1}{c}{16.4} &
                \multicolumn{1}{c}{$18.5 \pm 0.12 $} & \multicolumn{1}{c}{ 18.3 }
                \\\midrule

                \multirow{3}{*}{Crop 2} 
                & \multicolumn{1}{l}{Default} & \multicolumn{1}{c}{57.81} &
                \multicolumn{1}{c}{/}& \multicolumn{1}{c}{ / } \\
                & \multicolumn{1}{l}{QRDS} & \multicolumn{1}{c}{\textbf{16}} &
                \multicolumn{1}{c}{$16.7 \pm 0.03 $} & \multicolumn{1}{c}{ \textbf{16.7} }
                \\
                & \multicolumn{1}{l}{CMA-ES IPOP} & \multicolumn{1}{c}{17} &
                \multicolumn{1}{c}{$19 \pm 0.08 $} & \multicolumn{1}{c}{ 19 }
                \\\midrule


                \multirow{3}{*}{Crop 3} 
                & \multicolumn{1}{l}{Default} & \multicolumn{1}{c}{63.65} &
                \multicolumn{1}{c}{/} & \multicolumn{1}{c}{ / } \\
                & \multicolumn{1}{l}{QRDS} & \multicolumn{1}{c}{\textbf{17.5}} &
                \multicolumn{1}{c}{$18 \pm 0.03 $} & \multicolumn{1}{c}{ \textbf{17.9} }\\
                & \multicolumn{1}{l}{CMA-ES IPOP} & \multicolumn{1}{c}{19} &
                \multicolumn{1}{c}{$21.5 \pm 0.12 $} & \multicolumn{1}{c}{ 21.6 }
                \\\midrule


                \multirow{3}{*}{Crop 4} 
                & \multicolumn{1}{l}{Default} & \multicolumn{1}{c}{47.85} &
                \multicolumn{1}{c}{/}& \multicolumn{1}{c}{ / } \\
                & \multicolumn{1}{l}{QRDS} & \multicolumn{1}{c}{\textbf{14.2}} &
                \multicolumn{1}{c}{$15.8 \pm 0.06 $}& \multicolumn{1}{c}{ \textbf{16} } \\
                & \multicolumn{1}{l}{CMA-ES IPOP} & \multicolumn{1}{c}{16.7} &
                \multicolumn{1}{c}{$20 \pm 0.25 $}& \multicolumn{1}{c}{ 19.4 }
                \\\midrule



                \multirow{3}{*}{Crop 5} 
                & \multicolumn{1}{l}{Default} & \multicolumn{1}{c}{50.74} &
                \multicolumn{1}{c}{/} & \multicolumn{1}{c}{ / } \\
                & \multicolumn{1}{l}{QRDS} & \multicolumn{1}{c}{\textbf{15.1}} &
                \multicolumn{1}{c}{$15.3 \pm 0.02 $}& \multicolumn{1}{c}{ \textbf{15.3} } \\
                & \multicolumn{1}{l}{CMA-ES IPOP} & \multicolumn{1}{c}{15.3} &
                \multicolumn{1}{c}{$16.6 \pm 0.07$}& \multicolumn{1}{c}{ 16.5 }
                \\\bottomrule


            \end{tabular}
        %	\end{center}
    \end{table}
Le tableau~\ref{tab:results} présente les \gls{mse} avec leurs intervalles de 
confiance à 95\% de chaque algorithme ainsi que la performance du paramétrage 
par défaut. Une erreur faible indique une bonne prédiction. Quel que soit l'algorithme utilisé, la performance est toujours meilleure que le paramétrage par défaut. Ceci montre la pertinence de la méthode comme moyen de résolution de ce problème. Concernant la comparaison des algorithmes, la figure~\ref{fig:res_crop_optim} présente la dispersion des optima trouvés après 100 optimisations pour chaque champ et pour chaque algorithme. Autrement dit chaque algorithme a opéré 100 optimisations avec un budget d’ $10^6$ évaluations à chaque optimisation pour chaque champ. L'optimum à chaque optimisation est ensuite conservé. Ceci représente donc finalement 500 valeurs par algorithme. D'après les résultats, QRDS est toujours statistiquement meilleur que CMAES-IPOP au vu des performances. De plus, QRDS est plus fiable, car la dispersion de ses optima est plus faible, au contraire de CMAES-IPOP ayant une dispersion plus importante. Si l'on considère à présent la meilleure solution trouvée pour chaque algorithme après les 100 optimisations pour chaque champ (comme le montre la figure~\ref{fig:res_box_plot_optim}), QRDS reste statistiquement plus performante que CMAES-IPOP.
La raison de cette différence peut être expliquée par la topologie du problème. 
En effet, le problème étant très fortement multimodal, une stratégie 
favorisant une exploration semble plus efficace et QRDS grâce à son 
"murder operator" ne concentre son budget que sur des zones où un optimum n'a pas encore été découvert. Une autre hypothèse peut être la forme du paysage qui 
ne doit pas  être globalement convexe, ce qui n'aide pas la matrice de 
covariance de CMA à converger.

\subsection{Synthèse}
À partir de la reformulation d'un problème de paramétrage d'un modèle biologique en un problème d'optimisation multimodale boîte noire, résultats des  expériences démontrent que l'utilisation de l'analyse inverse comme méthodologie afin de paramétrer automatiquement le modèle par rapport à l'utilisation du paramétrage expert était pertinente puisque les jeux de paramètres issus de notre méthodologie sont toujours meilleurs pour prédire l'évolution de la teneur en eau dans le sol. Les tests ont été effectués sur 5 parcelles de cultures différentes sur plusieurs années. Le gain moyen est  de $23\%$ grâce à l'utilisation de cette méthode par rapport au paramétrage de référence. Concernant la comparaison de l'algorithme QRDS et de l'algorithme CMAES-IPOP, 100 exécutions de chaque algorithme ont été opérées sur chaque champ de test. Il en résulte que l'algorithme QRDS est statistiquement meilleur comme méthode de résolution. On peut émettre l'hypothèse qu'il est nécessaire d'explorer plus fréquemment l'espace de recherche et/ou que la fonction objective doit être localement simple (fonction lisse, localement convexe, \dots) ce qui avantage l'algorithme QRDS par rapport à CMA-ES qui doit utiliser une partie de ses évaluations pour l'apprentissage de sa matrice de covariance. Enfin la question de la fiabilité et de la robustesse des jeux de paramètres optimisés dans le contexte de la culture de pommes de terre a été étudiée. Expérimentalement, les performances de validation d'un jeu de paramètres optimisé sur les autres parcelles de tests (sans nouveau processus d'optimisation) apportent un gain statistiquement significatif par rapport au paramétrage de référence. Ceci permet de valider l'approche et la méthodologie employée.
